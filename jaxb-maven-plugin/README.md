# OpenJAX JAXB Maven Plugin

> Maven Plugin for JAX-B

[![Build Status](https://travis-ci.org/openjax/jaxb.png)](https://travis-ci.org/openjax/jaxb)
[![Coverage Status](https://coveralls.io/repos/github/openjax/jaxb/badge.svg)](https://coveralls.io/github/openjax/jaxb)
[![Javadocs](https://www.javadoc.io/badge/org.openjax.jaxb/jaxb-maven-plugin.svg)](https://www.javadoc.io/doc/org.openjax.jaxb/jaxb-maven-plugin)
[![Released Version](https://img.shields.io/maven-central/v/org.openjax.jaxb/jaxb-maven-plugin.svg)](https://mvnrepository.com/artifact/org.openjax.jaxb/jaxb-maven-plugin)

### Introduction

The `jaxb-maven-plugin` plugin is used for JAX-B.

### Goals Overview

* [`xjc:xjc`](#xjcxjc) XJC.

### License

This project is licensed under the MIT License - see the [LICENSE.txt](LICENSE.txt) file for details.

[mvn-plugin]: https://img.shields.io/badge/mvn-plugin-lightgrey.svg